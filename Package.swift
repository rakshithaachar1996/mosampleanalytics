// swift-tools-version:5.3
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "MOSampleAnalytics",
    products: [
        // Products define the executables and libraries a package produces, and make them visible to other packages.
        .library(
            name: "MOSampleAnalytics",
            targets: ["MOSampleAnalytics"]),
    ],
    dependencies: [
        .package(name:"MoEngageCore", url: "https://rakshithaachar1996@bitbucket.org/rakshithaachar1996/moengagecore.git", from: "1.0.0"),
        .package(name: "MOAnalyticsSample", url: "https://rakshithaachar1996@bitbucket.org/rakshithaachar1996/moanalyticssample.git", .branch("master")),
    ],
    targets: [
        .target(name: "MOSampleAnalytics",
                dependencies: ["MOAnalyticsSample", "MoEngageCore"])
     
    ]
)
